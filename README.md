## Fundamentos de GIT y GitLab

**Prerequisitos:**

Descargas:

- Visual Studio Code https://code.visualstudio.com/

- GIT https://git-scm.com/

- GitKraken https://www.gitkraken.com/

Enlaces:
- Editor Markdown https://stackedit.io/

- Bootstrap https://getbootstrap.com/docs/4.1/getting-started/download/

# Libro de trabajo

Configurar entorno de trabajo GIT:

`git config --global user.email <correo>`

`git config --global user.name <username>`

* * *

`git init`
Crea una base de datos, donde se almacena el historial de nuestro código.

`git status`
Enumera los archivos en stage que deben de ser confirmados para almacenarlos en la bd del repositorio.

`git add <nombre archivo>`
Agregar un archivo/ directorio a la base de datos y stage de commit.

`git commit -m "<mensaje>" `
Se guardan los cambios en la base de datos del repositorio. 

`git rm <nombre del archivo> `
Elimina el archivo de la base de datos del repositorio

`git diff <id commit>`
Muestra las diferencias entre el area de trabajo actual a el commit especificado

`git revert <id commit>`
Regresa el repositorio del proyecto a la versión del id del commit indicado

`git checkout <id commit>`
Viaja en el tiempo, crea una nueva rama con la versión de codigo del id del commit indicado 

`git remote add origin  <REMOTE_URL>`
Añade el repositorio local en la url indicada 

`git checkout <nombre rama>`
Crea una nueva rama con el nombre indicado como parámetro

`git merge <nombre>`
realiza la fusión de los cambios entre la rama del contexto y la rama que se indica en el parámetro 

## PRACTICA UNO 

1.
```
<!DOCTYPE html>
<html>
    <head>
        <h1>Hola Mundo!</h1>
    </head>
</html>
```
2.

```
<!DOCTYPE html>
<html>
    <head>
        <h1>Hola Mundo!</h1>
        <title>Hola Mundo</title>
    </head>
    <body>
        <h1>Hola rellena el formulario</h1>
        <form>
            <h3>Información de contacto</h3>
                <label>
                    Nombre:
                </label>
                <br>
                <input type="text"/>
                <br>
                <label>
                    Email:
                </label>
                <br>
                <input type="email"/>
                <br>
                <label>
                    Numero:
                </label>
                <br>
                <input type="number"/>
                <br>
                <label>
                    Motivo:
                </label>
                <br>
                <select>
                    <option>Dudas</option>
                    <option>Inversión</option>
                    <option>Otro</option>
                </select>
                <br>
                <label>
                    Mensaje:
                </label>
                <br>
                <textarea>
                </textarea>
                <br>
                <button>Enviar Información</button>
        </form>
    </body>
</html>
```



3.
```
<!DOCTYPE html>
<html>
    <head>
        <h1>Hola Mundo!</h1>
        <title>Hola Mundo</title>
    </head>
    <body>
        <h1>Hola rellena el formulario</h1>
        <form>
            <h3>Información de contacto</h3>
                <label>
                    Nombre:
                </label>
                <br>
                <input type="text"/>
                <br>
                <label>
                    Email:
                </label>
                <br>
                <input type="email"/>
                <br>
                <label>
                    Numero:
                </label>
                <br>
                <input type="number"/>
                <br>
                <label>
                    Motivo:
                </label>
                <br>
                <select>
                    <option>Dudas</option>
                    <option>Inversión</option>
                    <option>Otro</option>
                </select>
                <br>
                <label>
                    Mensaje:
                </label>
                <br>
                <textarea>
                </textarea>
                <br>
                <button>Enviar Información</button>
        </form>
    </body>
</html>
```

4.
Git add con stage de dos documentos:
 
 Crear carpeta y archivo **css/style.css**

```
 *{
    margin:5px;
}
body{
    background-color: #D4b16A;
}
label{
    font-weight: bold;
}
button{
    cursor: pointer;
}
```
Añadir link de css en index.html

`<link rel="stylesheet" href="./css/style.css"/>`


`git add css/style.css index.html`





















```
            <div class="container">
            
            <form class="row g-3">
                <div class="col-md-6">
                  <label for="inputEmail4" class="form-label">Email</label>
                  <input type="email" class="form-control" id="inputEmail4">
                </div>
                <div class="col-md-6">
                  <label for="inputPassword4" class="form-label">Password</label>
                  <input type="password" class="form-control" id="inputPassword4">
                </div>
                <div class="col-12">
                  <label for="inputAddress" class="form-label">Address</label>
                  <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                </div>
                <div class="col-12">
                  <label for="inputAddress2" class="form-label">Address 2</label>
                  <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                </div>
                <div class="col-md-6">
                  <label for="inputCity" class="form-label">City</label>
                  <input type="text" class="form-control" id="inputCity">
                </div>
                <div class="col-md-4">
                  <label for="inputState" class="form-label">State</label>
                  <select id="inputState" class="form-select">
                    <option selected>Choose...</option>
                    <option>...</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <label for="inputZip" class="form-label">Zip</label>
                  <input type="text" class="form-control" id="inputZip">
                </div>
                <div class="col-12">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck">
                    <label class="form-check-label" for="gridCheck">
                      Check me out
                    </label>
                  </div>
                </div>
                <div class="col-12">
                  <button type="submit" class="btn btn-primary">Sign in</button>
                </div>
              </form>
              </div>
```
